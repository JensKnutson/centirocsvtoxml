﻿using CentiroWebApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CentiroCsvToXml
{
    class CsvToXml
    {
        private readonly string xmlPath = @"C:\Resources\xml\orders.xml";

        public void ConvertToXml(String[] csvValues)
        {
            List<Order> orders = CreateOrders(csvValues);
            CreateXml(orders);
        }

        private void CreateXml(List<Order> orders)
        {
            using (var writer = XmlWriter.Create(xmlPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Order>));
                serializer.Serialize(writer, orders);
            }
        }

        private List<Order> CreateOrders(string[] csvValues)
        {
            List<Order> orders = new List<Order>();
            foreach (string csvValue in csvValues)
            {
                orders.Add(new Order(csvValue));
            }
            return orders;
        }
    }
}
