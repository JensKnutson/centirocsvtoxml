﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CentiroCsvToXml
{
    class GetOrdersFromCsv
    {
        private readonly string csvPath = @"C:\Resources\csv";

        public string[] getOrdersFromCsv()
        {
            string csv = CsvToString();
            string[] csvValues = RemoveHeaders(csv);
            csvValues = TrimValues(csvValues);
            return csvValues;
        }

        private string[] GetCsvFileLocation()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(csvPath, "*.txt");
                return filePaths;
            }
            catch (DirectoryNotFoundException e)
            {
                throw new DirectoryNotFoundException(e.StackTrace);
            }

        }

        private string CsvToString()
        {
            string csv = "";
            List<String> fileNames = ImportCsv();
            foreach (string files in fileNames)
            {
                csv += File.ReadAllText(files);
                csv += "\r\n";
            }
            return csv;
        }

        private List<string> ImportCsv()
        {
            List<string> fileNames = new List<string>();
            fileNames.AddRange(GetCsvFileLocation().ToList());
            return fileNames;
        }

        private string[] TrimValues(string[] CsvValues)
        {
            for (int i = 0; i < CsvValues.Length; i++)
            {
                CsvValues[i] = CsvValues[i].Remove(0, 1);
                CsvValues[i] = CsvValues[i].Remove(CsvValues[i].Length - 1, 1);
            }
            return CsvValues;
        }

        private string[] RemoveHeaders(string csv)
        {
            List<string> csvValues = csv.Split("\r\n").ToList();
            for (int i = 0; i < csvValues.Count(); i++)
            {
                if (csvValues[i].Contains("OrderNumber") || csvValues[i].Length < 2)
                {
                    csvValues.RemoveAt(i);
                }
            }
            return csvValues.ToArray();
        }
    }
}
