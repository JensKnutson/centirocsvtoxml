﻿using System;

namespace CentiroCsvToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            GetOrdersFromCsv csvOrderValues = new GetOrdersFromCsv();
            String[] orders = csvOrderValues.getOrdersFromCsv();
            CsvToXml csvToXml = new CsvToXml();
            csvToXml.ConvertToXml(orders);
        }
    }
}
