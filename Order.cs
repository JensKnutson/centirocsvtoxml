﻿using System;
using System.Globalization;

namespace CentiroWebApi.Models
{
    public class Order
    {
        public Order(String orderRow)
        {
            String[] orderRows = orderRow.Split("|");
            OrderNumber = Int32.Parse(orderRows[0]);
            OrderLineNumber = orderRows[1];
            ProductNumber = orderRows[2];
            Quantity = Int32.Parse(orderRows[3]);
            Name = orderRows[4];
            Description = orderRows[5];
            Price = Double.Parse(orderRows[6], CultureInfo.InvariantCulture);
            ProductGroup = orderRows[7];
            OrderDate = orderRows[8];
            CustomerName = orderRows[9];
            CustomerNumber = Int32.Parse(orderRows[10]);
        }

        public Order()
        {

        }

        public int OrderNumber { get; set; }
        public string OrderLineNumber { get; set; }
        public string ProductNumber { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string ProductGroup { get; set; }
        public string OrderDate { get; set; }
        public string CustomerName { get; set; }
        public int CustomerNumber { get; set; }
    }
}